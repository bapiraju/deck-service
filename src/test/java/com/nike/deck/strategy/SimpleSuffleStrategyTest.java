package com.nike.deck.strategy;

import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.hamcrest.core.IsEqual;
import org.hamcrest.core.IsNot;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.nike.deck.model.Deck;


@RunWith(SpringRunner.class)
@SpringBootTest
public class SimpleSuffleStrategyTest {

	@Autowired
	SimpleSuffleStrategy simpleSuffleStrategy;

	@Test
	public void testSuffle() {
		List<String> cards =  new Deck().getCards();
		List<String> copy = new ArrayList<>(cards);
		simpleSuffleStrategy.suffle(cards);
		assertThat(cards, IsNot.not(IsEqual.equalTo(copy)));
	}
}
