package com.nike.deck.controller;

import static org.junit.Assert.assertTrue;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DeckControllerTest {

	@Autowired
	private TestRestTemplate restTemplate;

	private final String URL = "/decks";

	private final String name = "/second";

	@Test
	public void testA_Put(){
		ResponseEntity<String> response = this.restTemplate.exchange(URL+name, HttpMethod.PUT, null, String.class);
		assertTrue(response.getStatusCode() == HttpStatus.OK);
		assertEquals("Success", response.getBody());
	}

	@Test
	public void testB_Get(){
		String response = this.restTemplate.getForObject(URL+name, String.class);
		assertThat(response).contains("5-heart");
	}

	@Test
	public void testC_Post(){
		ResponseEntity<String> response = this.restTemplate.exchange(URL+name, HttpMethod.POST, null, String.class);
		assertTrue(response.getStatusCode() == HttpStatus.OK);
		assertEquals("Success", response.getBody());
	}

	@Test
	public void testD_GetAll(){
		String response = this.restTemplate.getForObject(URL, String.class);
		assertThat(response).contains("second");
	}

	@Test
	public void testE_Delete(){
		ResponseEntity<String> response = this.restTemplate.exchange(URL+name, HttpMethod.DELETE, null, String.class);
		assertTrue(response.getStatusCode() == HttpStatus.OK);
		assertEquals("Success", response.getBody());
	}
}
