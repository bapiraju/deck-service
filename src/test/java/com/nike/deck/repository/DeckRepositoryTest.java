package com.nike.deck.repository;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Map;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.nike.deck.model.Deck;

@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DeckRepositoryTest {

	@Autowired
	IDeckRepository deckRepository;

	private final String deckName = "first";

	@Test
	public void testA_Create(){
		String message = deckRepository.create(deckName);
		assertTrue("Success".equals(message));
	}

	@Test
	public void testB_Suffle() {
		String message = deckRepository.shuffle(deckName);
		assertTrue("Success".equals(message));
	}

	@Test
	public void testC_ListAll(){
		Map<String, Deck> decks = deckRepository.listAll();
		assertNotNull(decks.get(deckName));
		assertTrue(decks.size() == 1);
	}

	@Test
	public void testD_GetByName(){
		Deck deck = deckRepository.getByName(deckName);
		assertNotNull(deck);
	}

	@Test
	public void testE_Delete(){
		String message = deckRepository.delete(deckName);
		assertTrue("Success".equals(message));
		Deck deck = deckRepository.getByName(deckName);
		assertNull(deck);
	}
}
