package com.nike.deck.model;

import java.util.ArrayList;
import java.util.List;

public class Deck {

	String[] suits = { "heart", "spade", "dimond", "club" };
	String[] numbers = { "A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K" };

	private List<String> cards = new ArrayList<>();

	public Deck() {

		for (int i = 0; i < suits.length; i++) {
			for (int j = 0; j < numbers.length; j++) {
				cards.add(numbers[j] + "-" + suits[i]);
			}
		}
	}

	public List<String> getCards() {
		return cards;
	}
}
