package com.nike.deck.repository;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.nike.deck.model.Deck;
import com.nike.deck.strategy.SuffleStrategyContext;

@Repository
public class DeckRepository implements IDeckRepository {

	Map<String, Deck> decks = new HashMap<>();

	private final String successMessage = "Success";
	private final String errorMessage = "failed";

	@Autowired
	private SuffleStrategyContext context;

	@Override
	public String create(String name) {
		decks.put(name, new Deck());
		return successMessage;
	}

	@Override
	public String shuffle(String name) {
		Deck deck = decks.get(name);
		if (deck == null)
			return errorMessage;
		context.suffle(deck.getCards());
		return successMessage;
	}

	@Override
	public Map<String, Deck> listAll() {
		return decks;
	}

	@Override
	public Deck getByName(String name) {
		return decks.get(name);
	}

	@Override
	public String delete(String name) {
		decks.remove(name);
		return successMessage;
	}
}
