package com.nike.deck.repository;

import java.util.Map;

import com.nike.deck.model.Deck;

public interface IDeckRepository {

	String create(String name);

	String shuffle(String name);

	Map<String, Deck> listAll();

	Deck getByName(String name);

	String delete(String name);

}