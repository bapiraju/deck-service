package com.nike.deck.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nike.deck.model.Deck;
import com.nike.deck.repository.IDeckRepository;

@Service
public class DeckService implements IDeckService {

	@Autowired
	private IDeckRepository deckRepository;

	@Override
	public String create(String name) {
		return deckRepository.create(name);
	}

	@Override
	public String shuffle(String name) {
		return deckRepository.shuffle(name);
	}

	@Override
	public Map<String, Deck> listAll() {
		return deckRepository.listAll();
	}

	@Override
	public Deck getByName(String name) {
		return deckRepository.getByName(name);
	}

	@Override
	public String delete(String name) {
		return deckRepository.delete(name);
	}
}
