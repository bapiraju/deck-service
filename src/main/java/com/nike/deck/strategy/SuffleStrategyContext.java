package com.nike.deck.strategy;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class SuffleStrategyContext {

	@Autowired
	private ApplicationContext appContext;

	@Value("${shuffle.algorithm}")
	private String algorithm;

	public void suffle(List<String> cards){
		ISuffleStrategy strategy = appContext.getBean(algorithm, ISuffleStrategy.class);
		strategy.suffle(cards);
	}
}
