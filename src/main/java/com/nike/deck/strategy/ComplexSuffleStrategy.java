package com.nike.deck.strategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.springframework.stereotype.Component;

@Component("ComplexStrategy")
public class ComplexSuffleStrategy implements ISuffleStrategy {

	@Override
	public void suffle(List<String> cards) {
		int suffleTimes = ThreadLocalRandom.current().nextInt(10, 20 + 1);
		List<String> suffled = new ArrayList<>(cards);
		while (suffleTimes > 0) {
			List<List<String>> partitions = doHalf(suffled);
			suffled = interleave(partitions.get(0), partitions.get(1));
			suffleTimes--;
		}
		cards.clear();
		cards.addAll(suffled);
	}

	private List<List<String>> doHalf(List<String> originalList) {
		int partitionSize = originalList.size() / 2;
		List<List<String>> partitions = new LinkedList<List<String>>();
		for (int i = 0; i < originalList.size(); i += partitionSize) {
			partitions.add(originalList.subList(i, Math.min(i + partitionSize, originalList.size())));
		}
		return partitions;
	}

	// This method can be implemented better using iterator
	private List<String> interleave(List<String> first, List<String> second) {
		List<String> result = new ArrayList<String>(first.size() + second.size());

		Iterator<String> firstItr = first.iterator();
		Iterator<String> secondItr = second.iterator();
		while (firstItr.hasNext() || secondItr.hasNext()) {
			if (firstItr.hasNext())
				result.add(firstItr.next());
			if (secondItr.hasNext())
				result.add(secondItr.next());
		}
		Collections.shuffle(result);
		return result;

	}
}
