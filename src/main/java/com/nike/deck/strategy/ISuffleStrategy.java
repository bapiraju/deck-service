package com.nike.deck.strategy;

import java.util.List;

public interface ISuffleStrategy {

	void suffle(List<String> cards);
}
