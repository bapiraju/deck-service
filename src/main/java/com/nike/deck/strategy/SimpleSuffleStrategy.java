package com.nike.deck.strategy;

import java.util.Collections;
import java.util.List;

import org.springframework.stereotype.Component;

@Component("SimpleStrategy")
public class SimpleSuffleStrategy implements ISuffleStrategy {

	@Override
	public void suffle(List<String> cards) {
		Collections.shuffle(cards);
	}

}
