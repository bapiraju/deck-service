package com.nike.deck.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.nike.deck.model.Deck;
import com.nike.deck.service.IDeckService;

@RestController
@RequestMapping(value = "/decks")
public class DeckController {

	@Autowired
	private IDeckService deckService;

	@RequestMapping(value = "/{name}", method = RequestMethod.PUT)
	public String create(@PathVariable("name") String name) {
		return deckService.create(name);
	}

	@RequestMapping(value = "/{name}", method = RequestMethod.POST)
	public String shuffle(@PathVariable("name") String name) {
		return deckService.shuffle(name);
	}

	@RequestMapping(method = RequestMethod.GET)
	public Map<String, Deck> listAll() {
		return deckService.listAll();
	}

	@RequestMapping(value = "/{name}", method = RequestMethod.GET)
	public Deck getByName(@PathVariable("name") String name) {
		return deckService.getByName(name);
	}

	@RequestMapping(value = "/{name}", method = RequestMethod.DELETE)
	public String delete(@PathVariable("name") String name) {
		return deckService.delete(name);
	}
}
