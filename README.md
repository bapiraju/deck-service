#Deck-Service
 * RESTful microservice that implements a card shuffling algorithm

##Instructions for building/running/using the microservice.

  * **Executing testcases**
       * `gradle test`

  * **Building Service**
       * `gradle build`

  * **Executing Service**
       * `gradle run`

##API Endpoints.
     
	* PUT an idempotent request for the creation of a new named deck.
		* PUT http://localhost:9090/decks/{name}

	* POST a request to shuffle an existing named deck
		* POST http://localhost:9090/decks/{name}

	* GET a list of the current decks persisted in the service.
		* GET http://localhost:9090/decks

	* GET a named deck in its current sorted/shuffled order.
		* GET http://localhost:9090/decks/{name}

	* DELETE a named deck.
		* DELETE http://localhost:9090/decks/{name}
       